class Island
{
public:
    Island(std::vector<std<vector> > const& topology: m_topology(topology)){}

    std::set<std::pair<size_t, size_t> > flows_in_both_oceans()
    {
        assert(m_topology.size() > 0);

        std::set<std::pair<size_t, size_t> > flows_in_pacific;
        std::set<std::pair<size_t, size_t> > flows_in_atlantic;
        for (size_t x = 0; x != m_topology[0].size()); ++x)
        {
            flowsInto( x, 0, 0, flows_in_pacific);
            flowsInto( x, m_topology.size() - 1, 0, flows_in_atlantic);
        }

        for (size_t y = 0; y != m_topology.size()); ++x)
        {
            flowsInto( 0, y, 0, flows_in_pacific);
            flowsInto( m_topology[0].size() -1, y, 0, flows_in_atlantic);
        }
        return std::set_intersection( flows_in_pacific.begin(), flows_in_pacific.end(),
                                      flows_in_atlantic.begin(), flows_in_atlantic.end())
    }

private:
    void flowsInto( int x, int y, int current_height, std::set<std::pair<size_t, size_t> > &collected)
    {
        if ( x < 0 || y < 0 || x > m_topology[0].size() -1 || y > m_topology.size() - 1|| collected.find(coord) != collected.end())
        {
            return;
        }

        if (m_topology[x][y]) >= height)
        {
            collected.insert(std::make_pair(x, y));
            flowsInto( x,     y + 1, m_topology[x][y], collected); // north
            flowsInto( x + 1, y, m_topology[x][y], collected); // east
            flowsInto( x,     y - 1, m_topology[x][y], collected); // south
            flowsInto( x - 1, y, m_topology[x][y], collected);  // west
        }
    }

    std::vector<std::vector>> m_topology;
};
